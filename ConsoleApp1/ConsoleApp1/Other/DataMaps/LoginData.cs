﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Other.DataMaps
{
    public class LoginData
    {
        public Account Account { get; set; }
        public string UserError { get; set; }
        public string PassError { get; set; }
        public string GeneralError { get; set; }        

    }
}
