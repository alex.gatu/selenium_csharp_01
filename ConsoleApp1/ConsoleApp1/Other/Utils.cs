﻿using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.Office.Interop.Excel;
using static ConsoleApp1.Other.EnumItems;
using ConsoleApp1.Other.DataMaps;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Data;
using ConsoleApp1.Properties;
using System.Diagnostics;

namespace ConsoleApp1
{
    public class Utils
    {
        public static string msConString = String.Format("Server={0};Database={1};Integrated Security={2}",
                    Properties.CustomSettings.Default.MsSqlServer,
                    Properties.CustomSettings.Default.MsSqlDbName,
                    Properties.CustomSettings.Default.MsSecurity);

        public static IWebDriver GetDriver(Browsers driverType)
        {
            IWebDriver driver = null;
            switch (driverType)
            {
                case Browsers.Firefox:
                    {
                        try
                        {
                            FirefoxProfile profile = new FirefoxProfile();
                            //profile.AddExtension(@"C:\Users\Alex\AppData\Roaming\Mozilla\Firefox\Profiles\72sgb8wg.default\extensions\cookie-manager@robwu.nl.xpi");
                            profile.SetPreference("acceptsInsecureCerts", true);
                            profile.AcceptUntrustedCertificates = true;
                            FirefoxOptions fo = new FirefoxOptions()
                            {
                                BrowserExecutableLocation = @"C:\Program Files\Mozilla Firefox\firefox.exe",
                                Profile = profile
                            };
                            //fo.AddAdditionalCapability("browserName","ff");
                            //fo.AddArgument("--headless");
                            return new FirefoxDriver(fo);
                        }
                        catch
                        {
                            return new FirefoxDriver();
                        }
                    }
                case Browsers.Chrome:
                    {
                        ChromeOptions co = new ChromeOptions();
                        //co.AddArgument("--start-maximized");
                        //co.AddArgument("--headless");
                        return new ChromeDriver(co);
                    }
                case Browsers.Edge:
                    {
                        return new EdgeDriver();
                    }
                default:
                    {
                        Console.WriteLine("Driver selected not supported!");
                        break;
                    }
            }
            return driver;
        }

        public static string ReadFile(string file)
        {
            return File.ReadAllText(file);
        }

        public static LoginData DeserilizeJson(string json_text)
        {
            if (!string.IsNullOrEmpty(json_text))
            {
                return JsonConvert.DeserializeObject<LoginData>(json_text);
            }
            return new LoginData();
        }

        public static LoginData DeserilizeXml(string xml_text)
        {
            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "loginData"
            };
            //XmlRootAttribute xRoot = new XmlRootAttribute();
            //xRoot.ElementName = "loginData";

            StringReader sr = new StringReader(xml_text);
            XmlSerializer xs = new XmlSerializer(typeof(LoginData), xRoot);

            return (LoginData)xs.Deserialize(sr);
        }

        public static IEnumerable<LoginDataCsv> ReadCsv(string file)
        {
            IEnumerable<LoginDataCsv> data = File.ReadAllLines(file)
                .Skip(1)
                .Select(x => x.Split(','))
                .Select(x => new LoginDataCsv(x[0], x[1], x[2], x[3], x[4]));
            return data;
        }

        public static List<LoginDataCsv> ReadExcel(string file, string sheetName)
        {
            List<LoginDataCsv> dataSet = new List<LoginDataCsv>();
            Application app = new Application();
            Sheets sh = app.Workbooks.Open(file).Worksheets;
            Range usedRange = sh[sheetName].UsedRange;
            try
            {
                if (usedRange.Rows.Count > 1)
                {
                    for (int i = 2; i < usedRange.Rows.Count; i++)
                    {
                        LoginDataCsv currentRowData = new LoginDataCsv(
                            usedRange.Cells[i, 2].Value2.ToString(),
                            usedRange.Cells[i, 3].Value2.ToString(),
                            usedRange.Cells[i, 4].Value2.ToString(),
                            usedRange.Cells[i, 5].Value2.ToString(),
                            usedRange.Cells[i, 6].Value2.ToString()
                            );
                        dataSet.Add(currentRowData);
                    }
                }
            }
            catch { }
            finally
            {
                app.Quit();
            }
            return dataSet;
        }

        public static IEnumerable<LoginDataCsv> GetCsvDataSet()
        {
            return Utils.ReadCsv(@"C:\Users\Alex\source\repos\NewRepo\ConsoleApp1\ConsoleApp1\TestData\Csv\accountData.csv");
        }

        public static System.Data.DataTable GetDataTable(string query, string tableName)
        {
            System.Data.DataTable tbl;
            using (SqlConnection conn = new SqlConnection(msConString))
            {
                SqlDataAdapter sda = new SqlDataAdapter(query, conn);
                DataSet ds = new DataSet(tableName);
                sda.Fill(ds, tableName);
                tbl = ds.Tables[tableName];
            }
            return tbl;
        }

        public static IEnumerable<LoginDataCsv> GetDbCredentials()
        {
            string query = "SELECT * FROM [testDb].[dbo].[Credentials]";
            System.Data.DataTable tbl = GetDataTable(query, "Credentials");
            foreach (DataRow rw in tbl.Rows)
            {
                string username = rw["username"].ToString();
                string password = rw["password"].ToString();
                yield return new LoginDataCsv(username, password, "", "", "");
            }
        }

        public static IEnumerable<LoginDataCsv> GetDbCredentials2()
        {
            IList<LoginDataCsv> myList = new List<LoginDataCsv>();
            string query = "SELECT * FROM [testDb].[dbo].[Credentials]";
            System.Data.DataTable tbl = GetDataTable(query, "Credentials");
            foreach (DataRow rw in tbl.Rows)
            {
                string username = rw["username"].ToString();
                string password = rw["password"].ToString();
                myList.Add(new LoginDataCsv(username, password, "", "", ""));
            }
            return myList;
        }

        public static void SaveLogFile(object method, Exception e)
        {
            string location = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\";
            try
            {
                using(StreamWriter sr = new StreamWriter(new FileStream(location + @"log.txt", FileMode.Append, FileAccess.Write, FileShare.ReadWrite)))
                {
                    sr.WriteLine(String.Format("{0} ({1}) - Method: {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), method.ToString()));
                    sr.WriteLine(e.ToString());
                    sr.WriteLine("");
                }
            }
            catch(IOException ioex)
            {
                if (!File.Exists(location + @"log.txt"))
                {
                    File.Create(location + @"log.txt");
                }

            }
        }

        public static void EventLogWriterCategory(object debugMessage, string appName, string appCat)
        {
            //EventLog.CreateEventSource(appName, "");
            if (!EventLog.SourceExists(appCat))
            {
                EventLog.CreateEventSource(appName, appCat);
            }
            if (Settings.Default.debugEnable)
                using (EventLog el = new EventLog())
                {
                    el.Source = appCat;
                    el.WriteEntry(debugMessage.ToString(), EventLogEntryType.Information);
                }

            //EventLog.CreateEventSource("TestApp", "");
            if (!EventLog.SourceExists("TestApp"))
            {
                EventLog.CreateEventSource("Test", "TestApp");
            }
            EventLog eventLog = new EventLog();
            eventLog.Source = "Test";

            eventLog.WriteEntry("You need community. It’s here and it’s waiting just for you. ", EventLogEntryType.Information);

        }

        public static void EventLogWriter(object debugMessage, int id, short category)
        {
            if (Settings.Default.debugEnable)
                using (EventLog el = new EventLog("Application"))
                {
                    el.Source = "Application";
                    el.WriteEntry(debugMessage.ToString(), EventLogEntryType.Information, id, category);
                }
        }

        public static void DebugWritter(object debugMessage, string type)
        {
            //if (Settings.Default.debugEnable)
            //    Console.WriteLine(String.Format("{0}{1} {2}", DateTime.Now, type, debugMessage.ToString()));
            if (String.Compare(System.Environment.GetEnvironmentVariable("DEBUG"), "True") == 0)
                Console.WriteLine(String.Format("{0}{1} {2}", DateTime.Now, type, debugMessage.ToString()));

        }

        public static float DivideNumbers(int x, int y)
        {
            //Console.WriteLine("[DEBUG] Element 1 " + x);
            DebugWritter("Element 1 " + x, "[DEBUG]");
            //Console.WriteLine("[DEBUG] Element 2 " + y);
            DebugWritter("Element 2 " + y, "[DEBUG]");
            float z = 0;
            try
            {
                z = x / y;
            }
            catch (Exception ex)
            {
                //DebugWritter(ex.Message, "[ERROR]");
                //DebugWritter(ex.StackTrace, "[INFO]");
                EventLogWriter(ex.Message, 1, 1);
                EventLogWriter(ex.StackTrace, 2, 3);
                EventLogWriterCategory(ex.Message, "ConsoleApp1", "Messages");
                EventLogWriterCategory(ex.StackTrace, "ConsoleApp1", "StackTraces");
                SaveLogFile(ex.Message, ex);
            }
            return z;
        }
    }
}
