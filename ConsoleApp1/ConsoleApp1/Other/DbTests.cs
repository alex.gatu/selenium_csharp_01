﻿
using MySql.Data.MySqlClient;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Tests
{
    //[TestFixture]
    public class DbTests : BaseClass
    {

        //[Test]
        public void MsSqlTest()
        {
            SqlCommand cmd = new SqlCommand("Select * FROM [testDb].[dbo].[Countries]", conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3));
            }
            reader.Close();

            SqlDataAdapter countries = new SqlDataAdapter("Select * FROM [testDb].[dbo].[Countries]", conn);
            DataSet ds = new DataSet("Countries");
            countries.Fill(ds, "Countries");
            DataTable tlb = ds.Tables["Countries"];
            foreach(DataRow drow in tlb.Rows)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", drow["id"].ToString(), drow["name"].ToString(), drow["shortname2"].ToString(), drow["shortname3"].ToString());
            }
        }

        //[Test]
        public void MySqlTest()
        {
            string query = "SELECT * FROM world.city;";
            MySqlCommand cmd = new MySqlCommand(query, myConn);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                Console.WriteLine("{0}, {1}, {2}", dataReader["ID"], dataReader["Name"], dataReader["CountryCode"]);
            }
            dataReader.Close();
        }

        //[Test]
        public void MsSqlInsertTest()
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = conn;          
                command.CommandType = CommandType.Text;
                command.CommandText = "INSERT into [testDb].[dbo].[Countries] (id, name, shortname2, shortname3) VALUES (3, @name, @shortname2, @shortname3)";
                command.Parameters.AddWithValue("@name", "Romania");
                command.Parameters.AddWithValue("@shortname2", "RO");
                command.Parameters.AddWithValue("@shortname3", "ROU");

                try
                {
                    int recordsAffected = command.ExecuteNonQuery();
                }
                catch (SqlException sqle)
                {
                    // error here
                    Console.WriteLine(sqle.Message);
                }

            }
        }
    }
}
