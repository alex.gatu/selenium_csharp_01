﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class LoginPage2
    {
        [FindsBy(How = How.Id, Using = "username")]
        IWebElement name;

        [FindsBy(How = How.Id, Using = "password")]
        IWebElement password;

        [FindsBy(How = How.ClassName, Using = "btn")]
        IWebElement submitButton;

        IWebElement x;

        public void Login(string user, string pass)
        {
            name.Clear();
            name.SendKeys(user);
            password.Clear();
            password.SendKeys(pass);
            //password.SendKeys(Keys.Enter);
            submitButton.Submit();
        }

        public IWebElement PopulateElement(IWebDriver driver)
        {
            return driver.FindElement(By.Id("ceva"));
        }
    }
}
