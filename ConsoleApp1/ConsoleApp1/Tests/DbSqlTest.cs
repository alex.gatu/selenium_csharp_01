﻿using ConsoleApp1.Other.DataMaps;
using MySql.Data.MySqlClient;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1.Tests
{
    [TestFixture]
    public class DbSqlTest : BaseClass
    {
        [Test]
        public void MsSqlSelectTest()
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM [testDb].[dbo].[Credentials]", conn);
                DataSet ds = new DataSet("Credentials");
                sda.Fill(ds, "Credentials");
                DataTable tbl = ds.Tables["Credentials"];
                foreach (DataRow dr in tbl.Rows)
                {
                    string username = dr["username"].ToString();
                    string password = dr["password"].ToString();
                    Console.WriteLine("{0}, {1}, {2}, {3}", dr["id"], username, password, dr["message"]);
                    driver.Navigate().GoToUrl("https://filelist.ro/login.php");
                    LoginPage lp = new LoginPage(driver);
                    lp.Login(username, password);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        } 
        
        [Test]
        public void MsSqlInsertTest()
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO [testDb].[dbo].[Credentials] " +
                    "(username, password, message) VALUES (@username, @password, @message)";
                cmd.Parameters.AddWithValue("@username", "gigel.fronel");
                cmd.Parameters.AddWithValue("@password", "vineoaiapapalupul");
                cmd.Parameters.AddWithValue("@message", "gigel.fronel");
                try
                {
                    int affectedRows = cmd.ExecuteNonQuery();
                }
                catch(SqlException sqle)
                {
                    Console.WriteLine(sqle.Message);
                }
            }
        }

        [Test]
        public void MySqlSelectTest()
        {
            string query = "SELECT * FROM world.city";
            MySqlCommand cmd = new MySqlCommand(query, myConn);
            using (MySqlDataReader mdr = cmd.ExecuteReader())
            {
                while (mdr.Read())
                {
                    Console.WriteLine("Name {0}, Country {1}", mdr["Name"], mdr["CountryCode"]);
                }
            }
        }

    }
}
