﻿using ConsoleApp1.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1.Tests
{
    [TestFixture]
    public class AdvancedSeleniumTests : BaseClass
    {        

        [Test]
        public void AlertTest()
        {
            driver.Navigate().GoToUrl(@"C:\Users\Alex\Desktop\Materiale SIIT\Test Automation C#\Code\popups.html");
            PopupsPage pp = new PopupsPage(driver);
            pp.AlertClick();
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();
            Thread.Sleep(2000);
        }

        [Test]
        public void ConfirmTest()
        {
            driver.Navigate().GoToUrl(@"file:///C:/Users/Alex/Desktop/web-stubs-master/stubs/popups.html");
            PopupsPage pp = new PopupsPage(driver);
            pp.ConfirmClick();
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();

            Thread.Sleep(2000);
        }

        [Test]
        public void PromptTest()
        {
            driver.Navigate().GoToUrl(@"file:///C:/Users/Alex/Desktop/web-stubs-master/stubs/popups.html");
            string inputString = "asjdoajsdkasdiasdpasda";
            PopupsPage pp = new PopupsPage(driver);
            pp.PromptClick();
            IAlert alert = driver.SwitchTo().Alert();
            alert.SendKeys(inputString);
            alert.Accept();
            Console.WriteLine(pp.ShowDiv());
            Assert.AreEqual(pp.ShowDiv(), inputString);

            Thread.Sleep(2000);
        }

        [Test]
        public void StaleTest()
        {
            driver.Navigate().GoToUrl(@"file:///C:/Users/Alex/Desktop/web-stubs-master/stubs/stale.html");
            StalePage sp = new StalePage(driver);
            sp.ClickMultiTimes(10);
            //IWebElement staleButton = driver.FindElement(By.Id("stale-button"));

            //for (int i = 0; i <= 10; i++)
            //{
            //    // We locate the element again before the action we need to perform (click)        
            //    staleButton = driver.FindElement(By.Id("stale-button"));
            //    staleButton.Click();
            //}
            Thread.Sleep(2000);
            ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(@"E:\Screenshots\testimage", ScreenshotImageFormat.Png);

            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            Console.WriteLine((string)js.ExecuteScript("return document.title"));
            

        }

        [Test]
        public void ClickStealer()
        {
            driver.Navigate().GoToUrl(@"file:///C:/Users/Alex/Desktop/web-stubs-master/stubs/thieves.html");
            ClickStealerPage csp = new ClickStealerPage(driver);
            csp.ClickStealerMultiTimes(4);

            Thread.Sleep(2000);
        }
    }
}
