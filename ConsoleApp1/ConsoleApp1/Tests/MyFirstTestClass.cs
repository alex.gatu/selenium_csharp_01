﻿using NHtmlUnit;
using NHtmlUnit.Html;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace ConsoleApp1
{
    [TestFixture]
    public class MyFirstTestClass
    {

        const String path = "E:\\Drivers";
        IWebDriver driver;

        [Test]
        public void MyFirstTest()
        {
            driver = new ChromeDriver(path);
            driver.Url = "https://google.com";
            driver.Navigate();
            Thread.Sleep(1000);

            IWebElement el1 = driver.FindElement(By.Name("q"));
            IWebElement el2 = driver.FindElement(By.Id("lst-ib"));

            el1.SendKeys("univers");
            Thread.Sleep(1000);
            //el1.SendKeys(Keys.Enter);
            IWebElement butonEnter = driver.FindElement(By.Name("btnK"));
            butonEnter.Submit();

            Console.WriteLine(driver.FindElement(By.Id("resultStats")).Text);
            //IReadOnlyCollection<IWebElement> searchResults = driver.FindElements(By.ClassName("r"));
            IReadOnlyCollection<IWebElement> searchResults2 = driver.FindElements(By.XPath("//h3[@class='r']/a"));
            //IReadOnlyCollection<IWebElement> searchResults3 = driver.FindElements(By.TagName("h3"));

            //foreach (IWebElement elem in searchResults2)
            //{
            //    Console.WriteLine(elem.Text);
            //    Console.WriteLine(elem.GetAttribute("href"));
            //}

            IWebElement el3 = driver.FindElement(By.PartialLinkText("Editura Univers"));
            Console.WriteLine(el3.Text);

            //driver.Navigate().Back();
            //Thread.Sleep(2000);

            //driver.Navigate().Forward();
            //Thread.Sleep(2000);

            //driver.Navigate().Refresh();
            //Thread.Sleep(1000);

            driver.Quit();
        }

        [Test]
        public void NHtmlUnitTest()
        {
            WebClient client = new WebClient(BrowserVersion.CHROME);
            HtmlPage page = client.GetHtmlPage("https://en.wikipedia.org");
            Console.WriteLine(page.Body);
            Assert.AreEqual(200, page.WebResponse.StatusCode);
            client.Close();
        }


        [Test]
        public void ChromeTest()
        {
            driver = new ChromeDriver(path);
            driver.Navigate().GoToUrl("https://google.com");
            Thread.Sleep(1000);
            driver.Close();

            Utils.DebugWritter(Utils.DivideNumbers(7, 0),"[INFO]");

            

        }

        [Test]
        public void FirefoxTest()
        {
            driver = new FirefoxDriver(path);
            driver.Navigate().GoToUrl("https://google.com");
            Thread.Sleep(2000);
            driver.Close();
        }

        [Test]
        public void EdgeTest()
        {
            driver = new EdgeDriver(path);
            driver.Navigate().GoToUrl("https://google.com");
            Thread.Sleep(2000);
            driver.Close();
        }

        [Test]
        public void IeDriverTest()
        {
            driver = new InternetExplorerDriver(path);
            driver.Navigate().GoToUrl("https://google.com");
            Thread.Sleep(2000);
            driver.Quit();
        }

        [Test]
        public void DriverCloseTest()
        {
            driver = new ChromeDriver(path);
            driver.Navigate().GoToUrl(Path.Combine(path, "targetPage.html"));

            Thread.Sleep(1000);
            IWebElement submitButton = driver.FindElement(By.Id("submit1"));
            Console.WriteLine("Pages open: " + driver.WindowHandles.Count);
            foreach (string handle in driver.WindowHandles)
            {
                Console.WriteLine("Handle " + handle);
            }
            submitButton.Submit();

            Console.WriteLine("Pages open: " + driver.WindowHandles.Count);
            foreach (string handle in driver.WindowHandles)
            {
                Console.WriteLine("Handle " + handle);
            }
            Thread.Sleep(2000);

            driver.Quit();
        }

        public void WaitFindAndClick(IWebDriver parent, By by, int counter, int maxWait)
        {
            for (int v = 0; v < counter; counter++)
            {
                try
                {
                    Thread.Sleep(1000);
                    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(maxWait));
                    wait.Until(d => { return parent.FindElement(by); });
                    if (parent.FindElement(by).Displayed)
                    {
                        parent.FindElement(by).Click();
                        break;
                    }
                }
                catch { }
            }
        }
        
        [Test]
        public void LazyButtonTest()
        {
            driver = new ChromeDriver(path);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            driver.Navigate().GoToUrl(Path.Combine(path, "targetPage.html"));

            Thread.Sleep(1000);
            IWebElement lazyButton = new WebDriverWait(driver, TimeSpan.FromSeconds(6))
                .Until(x => x.FindElement(By.Id("lazyID")));
            lazyButton.Click();
            //IWebElement lazyButton = driver.FindElement(By.Id("lazyID"));
            Console.WriteLine(lazyButton.Location);

            lazyButton.Click();
            WaitFindAndClick(driver, By.Id("lazyID"), 1, 6);

            driver.Quit();
        }

    }
}
