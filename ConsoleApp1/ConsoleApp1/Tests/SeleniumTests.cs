﻿using AventStack.ExtentReports;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    [TestFixture]
    public class SeleniumTests : BaseClass
    {
        const String path = "E:\\Drivers";

        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Test]
        public void LoginPageTest()
        {                       
            driver.Navigate().GoToUrl("https://filelist.ro/login.php");
            LoginPage2 lp = new LoginPage2();
            PageFactory.InitElements(driver, lp);
            lp.Login("myuser","pass123");
            Console.WriteLine(dir);
            Console.WriteLine(fileName);
            Assert.AreEqual(2, 4);
            test.Log(Status.Info, "User attempted to login");
            //test.AddScreenCaptureFromPath(String.Format("{0}\\screeenshot.png", dir));
            
            
        }

        [Test]
        public void LoginPageTestNegative()
        {

            driver.Navigate().GoToUrl("https://filelist.ro/login.php");
            LoginPage2 lp = new LoginPage2();
            PageFactory.InitElements(driver, lp);
            lp.Login("myuser", "pass123negative");
            
        }

        [Test]
        public void LoginPageNewTest()
        {
            driver.Navigate().GoToUrl("https://filelist.ro/login.php");
            LoginPage lp = new LoginPage(driver);
            //lp.Login("myuser", "pass123negative");
            Thread.Sleep(3000);
        }

        [Test]
        public void CookieTest()
        {
            driver.Navigate().GoToUrl("https://google.com");
            //LoginPage lp = new LoginPage(driver);
            //lp.Login("myuser", "pass123negative");
            ICookieJar cookieJar = driver.Manage().Cookies;
            IList<Cookie> cookies = cookieJar.AllCookies;

            foreach (Cookie c in cookies)
            {
                Console.WriteLine(String.Format("Name: {0} Value{1}", c.Name , c.Value));
                log.Info(String.Format("Name: {0} Value{1}", c.Name, c.Value));
            }

            Cookie c1 = driver.Manage().Cookies.GetCookieNamed("CONSENT");
            Console.WriteLine(String.Format("Name: {0} Value{1}", c1.Name, c1.Value));

            Cookie c2 = new Cookie("myCookie", "aklsjdljdlwjdolqjolqdjl091uwsdj");
            driver.Manage().Cookies.AddCookie(c2);

            log.Info("Info log ");
            Thread.Sleep(3000);
        }


    }
}
