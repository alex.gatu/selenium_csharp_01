﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    [TestFixture]
    public class MyClassBackup
    {

        const String path = "E:\\Drivers";
        IWebDriver driver;
        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver(path);
        }

        [Test]
        public void MyFirstTest()
        {
            
            driver.Url = "https://google.com";
            driver.Navigate();
            Thread.Sleep(1000);

            IWebElement el1 = driver.FindElement(By.Name("q"));
            IWebElement el2 = driver.FindElement(By.Id("lst-ib"));

            el1.SendKeys("univers");
            Thread.Sleep(1000);
            //el1.SendKeys(Keys.Enter);
            IWebElement butonEnter = driver.FindElement(By.Name("btnK"));
            butonEnter.Submit();

            Console.WriteLine(driver.FindElement(By.Id("resultStats")).Text);
            //IReadOnlyCollection<IWebElement> searchResults = driver.FindElements(By.ClassName("r"));
            IReadOnlyCollection<IWebElement> searchResults2 = driver.FindElements(By.XPath("//h3[@class='r']/a"));
            //IReadOnlyCollection<IWebElement> searchResults3 = driver.FindElements(By.TagName("h3"));

            foreach (IWebElement elem in searchResults2)
            {
                Console.WriteLine(elem.Text);
                Console.WriteLine(elem.GetAttribute("href"));
            }

            //driver.Quit();
        }

        public void WaitFindAndClick(IWebDriver parent, By by, int counter)
        {
            bool displayed = false;
            for (int v = 0; v < counter; counter++)
            {
                try
                {
                    Thread.Sleep(1000);
                    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                    wait.Until((d) => { return parent.FindElement(by); });
                    displayed = parent.FindElement(by).Displayed;
                    if (displayed)
                    {
                        parent.FindElement(by).Click();
                        break;
                    }
                }
                catch
                {
                }
            }
        }


        [Test]
        public void DriverCloseTest()
        {
            //driver = new ChromeDriver(path);
            driver.Url = path + "\\targetPage.html";
            driver.Navigate();
            Thread.Sleep(1000);
            IWebElement el1 = driver.FindElement(By.Id("submit1"));
            Console.WriteLine("Pages open : " + driver.WindowHandles.Count);
            el1.Submit();
            Console.WriteLine("Pages open : " + driver.WindowHandles.Count);
            string newWindowHandle = driver.WindowHandles.Last();
            IWebDriver newWindow = driver.SwitchTo().Window(newWindowHandle);
            Console.WriteLine(newWindow.Title);
            Thread.Sleep(1000);
            driver.Close();
            Thread.Sleep(4000);
            //driver.Quit();
        }

        [Test]
        public void WaitsTest()
        {
            driver.Url = path + "\\targetPage.html";
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            driver.Navigate();
            IWebElement myDynamicElement = (new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(x => x.FindElement(By.Id("lazyID"))));
            driver.FindElement(By.Id("lazyID")).Click();
            IWebElement element = driver.FindElement(By.Id("lazyID"));
            Thread.Sleep(1000);
        }

        [TearDown]
        public void CleanUp()
        {
            driver.Quit();
        }

    }
}
