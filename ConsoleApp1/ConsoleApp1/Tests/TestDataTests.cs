﻿using ConsoleApp1.Other.DataMaps;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1.Tests
{
    [TestFixture]
    public class TestDataTests : BaseClass
    {

        [Test]
        public void JsonTest()
        {
            driver.Navigate().GoToUrl("https://filelist.ro/login.php");
            LoginPage lp = new LoginPage(driver);
            string json_text = Utils.ReadFile(@"C:\Users\Alex\source\repos\NewRepo\ConsoleApp1\ConsoleApp1\TestData\Json\accountData.json");
            LoginData ld = Utils.DeserilizeJson(json_text);
            lp.Login(ld.Account.Username, ld.Account.Password);
            Thread.Sleep(3000);
        }

        [Test]
        public void XmlTest()
        {
            driver.Navigate().GoToUrl("https://filelist.ro/login.php");
            LoginPage lp = new LoginPage(driver);
            string xml_text = Utils.ReadFile(@"C:\Users\Alex\source\repos\NewRepo\ConsoleApp1\ConsoleApp1\TestData\Xml\accountData.xml");
            Console.WriteLine(xml_text);
            LoginData ld = Utils.DeserilizeXml(xml_text);
            lp.Login(ld.Account.Username, ld.Account.Password);
            Thread.Sleep(3000);
        }                 

        [Test, TestCaseSource(typeof(Utils),"GetCsvDataSet")]
        public void CsvTest(LoginDataCsv currentData)
        {
            driver.Navigate().GoToUrl("https://filelist.ro/login.php");
            LoginPage lp = new LoginPage(driver);
            lp.Login(currentData.Username, currentData.Password);
            
            Thread.Sleep(3000);
        }

        [Test, TestCaseSource(typeof(Utils), "GetDbCredentials")]
        public void DbTest(LoginDataCsv currentData)
        {
            driver.Navigate().GoToUrl("https://filelist.ro/login.php");
            LoginPage lp = new LoginPage(driver);
            lp.Login(currentData.Username, currentData.Password);

            Thread.Sleep(3000);
        }

        [Test]
        public void ExcelTest()
        {
            driver.Navigate().GoToUrl("https://filelist.ro/login.php");
            LoginPage lp = new LoginPage(driver);
            List<LoginDataCsv> csvList = Utils.ReadExcel(@"C:\Users\Alex\source\repos\NewRepo\ConsoleApp1\ConsoleApp1\TestData\Excel\accountData.xlsx", "Sheet1");
            if (csvList.Count() > 0)
            {
                LoginDataCsv currentData = csvList.ElementAt(0);
                lp.Login(currentData.Username, currentData.Password);
            }

            Thread.Sleep(3000);
        }


    }
}
