﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1.Pages
{
    public class ClickStealerPage
    {
        IWebDriver driver;
        WebDriverWait wait;


        public ClickStealerPage(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, new TimeSpan(0, 0, 30));
        }

        IWebElement StealButton => wait.Until(SeleniumExtras.WaitHelpers.
                ExpectedConditions.ElementIsVisible(By.Id("the_button")));
        //driver.FindElement(By.Id("the_button"));

        public void ClickStealerMultiTimes(int times)
        {
            for (int i = 0; i < times; i++)
            {
                StealButton.Click();
                Actions act = new Actions(driver);
                Thread.Sleep(1000);
                act.SendKeys(Keys.Enter).Build().Perform();
                Thread.Sleep(1000);
                act.SendKeys(Keys.Escape).Build().Perform();
                Thread.Sleep(1000);

                
            }
        }

    }
}
