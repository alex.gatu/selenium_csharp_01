﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Pages
{
    public class StalePage
    {
        IWebDriver driver;

        public StalePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        IWebElement StaleButton => driver.FindElement(By.Id("stale-button"));

        public void ClickMultiTimes(int times)
        {
            for (int i =0; i < times; i++)
            {
                //IWebElement newStaleButton = driver.FindElement(By.Id("stale-button"));
                StaleButton.Click();
            }
        }
    }
}
