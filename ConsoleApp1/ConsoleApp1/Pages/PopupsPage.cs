﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Pages
{
    public class PopupsPage
    {
        IWebDriver driver;

        public PopupsPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        IWebElement AlertButton => driver.FindElement(By.Id("alertButton"));
        IWebElement ConfirmButton => driver.FindElement(By.Id("confirmButton"));
        IWebElement PromptButton => driver.FindElement(By.Id("promptButton"));
        IWebElement DivText => driver.FindElement(By.Id("outDiv"));

        public void AlertClick()
        {
            AlertButton.Click();
        }

        public void ConfirmClick()
        {
            ConfirmButton.Click();
        }

        public void PromptClick()
        {
            PromptButton.Click();
        }

        public string ShowDiv()
        {
            return DivText.Text;
        }

        public void HoverMe()
        {
            Actions actions = new Actions(driver);
            
            actions.MoveToElement(DivText).Build().Perform();

        }


    }
}
