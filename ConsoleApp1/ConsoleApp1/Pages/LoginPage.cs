﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class LoginPage
    {
        IWebDriver driver;
        
        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        IWebElement Username => driver.FindElement(By.Id("username"));
        IWebElement Password => driver.FindElement(By.Id("password"));
        IWebElement SubmitButton => driver.FindElement(By.ClassName("btn"));

        public void Login(string user, string pass)
        {
            Username.Clear();
            Username.SendKeys(user);
            Password.Clear();
            Password.SendKeys(pass);
            //password.SendKeys(Keys.Enter);
            SubmitButton.Submit();
        }
    }
}
