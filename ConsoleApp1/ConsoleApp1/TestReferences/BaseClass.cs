﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MySql.Data.MySqlClient;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ConsoleApp1.Other.EnumItems;

namespace ConsoleApp1
{
    [TestFixture]
    public class BaseClass
    {
        //const String path = "E:\\Drivers";
        public static IWebDriver driver;
        public static SqlConnection conn;
        public MySqlConnection myConn;

        string host = Properties.CustomSettings.Default.DbHostName;
        string uid = Properties.CustomSettings.Default.DbUsername;
        string pass = Properties.CustomSettings.Default.DbPassword;
        string schema = Properties.CustomSettings.Default.DbSchema;
        string connectionString = "";

        protected ExtentReports extent;
        protected ExtentTest test;

        public string dir;
        public string fileName;
        
        [OneTimeSetUp]
        public void BeforeAll()
        {
            dir = TestContext.CurrentContext.TestDirectory;
            //Console.WriteLine(dir);
            fileName = this.GetType().ToString();
            //Console.WriteLine(fileName);
            ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(String.Format("{0}\\{1}.html", dir, fileName));
            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
        }

        [OneTimeTearDown]
        public void AfterAll()
        {
            extent.Flush();
        }

        [SetUp]
        public void SetupTests()
        {
            Console.WriteLine("This is the setup ");
            driver = Utils.GetDriver(Browsers.Chrome);
            try
            {
                conn = new SqlConnection(Utils.msConString);
                conn.Open();
            }
            catch (SqlException sqle)
            {
                Console.WriteLine(sqle.Message);
            }

            connectionString = String.Format("SERVER={0};DATABASE={1};UID={2};PASSWORD={3};",
                host, schema, uid, pass);
            Console.WriteLine(connectionString);
            try
            {
                myConn = new MySqlConnection(connectionString);
                myConn.Open();
            }
            catch (MySqlException msqle)
            {
                Console.WriteLine(msqle.Message);
            }

            test = extent.CreateTest(TestContext.CurrentContext.Test.Name);
        }

        public Status MapStatuses (TestStatus status)
        {
            switch (status)
            {
                case TestStatus.Inconclusive:
                    return Status.Warning;
                case TestStatus.Skipped:
                    return Status.Skip;
                case TestStatus.Passed:
                    return Status.Pass;
                case TestStatus.Warning:
                    return Status.Warning;
                case TestStatus.Failed:
                    return Status.Fail;
                default:
                    return Status.Fatal;
            }
        }

        [TearDown]
        public void TearDownTests()
        {
            Console.WriteLine("This is the teardown");
            
            if (conn != null)
            {
                try
                {
                    conn.Close();
                }
                catch (SqlException sqle)
                {
                    Console.WriteLine(sqle.Message);
                }                
            }

            if (myConn != null)
            {
                try
                {
                    myConn.Close();
                }
                catch (MySqlException sqle)
                {
                    Console.WriteLine(sqle.Message);
                }

            }

            TestStatus status = TestContext.CurrentContext.Result.Outcome.Status;
            string stk = TestContext.CurrentContext.Result.StackTrace;

            //if (string.IsNullOrEmpty(stk))
            //{
            //    stk = "";
            //}
            //else
            //{
            //    stk = String.Format("The stack trace is : \n{0}", stk);
            //}

            stk = string.IsNullOrEmpty(stk) ? "" : String.Format("\nThe stack trace is : \n{0}", stk);
            Status logstatus = MapStatuses(status);
            test.Log(logstatus, "Test ended with " + logstatus + stk);
            if (logstatus == Status.Fail)
                test.Fail("Test failed", MediaEntityBuilder.CreateScreenCaptureFromPath(String.Format("{0}\\screeenshot.png", dir)).Build());

            extent.Flush();
            driver.Quit();
        }

    }
}
