﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApplication
{
    public interface IShape
    {
        void ComputeSize();
        string ToString();
        void ComputeSomething();

    }
}
