﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApplication
{
    [TestClass]
    public class MyTestClassMsTest
    {
        [ClassInitialize]
        public static void MsClassInit(TestContext c)
        {
            Console.WriteLine("class init");
        }

        [TestInitialize]
        public void MsTestInit()
        {
            Console.WriteLine("test init");
            
        }

        [TestMethod]
        public void MsTest1()
        {
            Console.WriteLine("ms test 1");
        }

        [TestMethod]
        public void MsTest12()
        {
            Console.WriteLine("ms test 1-2");
            
        }


        [TestMethod]
        public void MsTest2()
        {
            Console.WriteLine("ms test 2");
        }

        [TestCleanup]
        public void MsTestClean()
        {
            Console.WriteLine("ms test clean");
        }

        [ClassCleanup]
        public static void MsClassClean()
        {
            Console.WriteLine("class clean");
        }
    }
}
