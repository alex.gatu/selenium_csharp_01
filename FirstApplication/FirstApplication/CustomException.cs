﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApplication
{
    class CustomException : Exception
    {
        string message;

        public override string Message
        {
            get { return message; }
        }

        public CustomException()
        {

        }

        public CustomException(string message)
        {
            //Console.WriteLine("The message is {0}", message);
            this.message = message;
        }

    }
}
