﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApplication
{
    abstract class AbstractShape
    {
        protected int x; 

        public abstract void ComputeSize();
        public double GetArea()
        {
            //…
            return 0;
        }

    }

}
