﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApplication
{
    class Circle : Shape
    {

        public new void ComputeSomething()
        {
            Console.WriteLine("Circle compute method");
        }

        public override string ToString()
        {
            return "Circle area is " + base.ToString();
        }

        public void Test()
        {
            ComputeSomething();
        }
    }
}
