﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FirstApplication
{
    [TestFixture]
    public class MyFirstTest
    {
        public String driverExe = "E:\\";

        [Test] // NUnit attribute
        public void MyFirstTestMethod()
        {
            // initialize a the driver object using “new” keyword
            IWebDriver driver = new ChromeDriver(@driverExe);
            driver.Url = "http://www.google.com";
            // navigate to the address and exit the driver
            driver.Navigate();
            Thread.Sleep(1000);
            IWebElement we = driver.FindElement(By.Name("q"));
            we.SendKeys("koala");
            Thread.Sleep(500);
            we.SendKeys(Keys.Enter);
            Thread.Sleep(2000);
            driver.Quit();
        }


        [Test] // NUnit attribute
        public void SearchGoogle()
        {
            IWebDriver driver = new ChromeDriver(@driverExe);
            driver.Url = "http://www.google.com";
            driver.Navigate();
            Thread.Sleep(1000);
            IWebElement we = driver.FindElement(By.Name("q"));
            we.SendKeys("koala");
            we.SendKeys(Keys.Enter);
            IWebElement myDynamicElement = (new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(x => x.FindElement(By.Id("resultStats"))));
            Console.WriteLine("Text: " + driver.FindElement(By.Id("resultStats")).Text);

            IReadOnlyCollection<IWebElement> resultTitleElements = driver.FindElements(By.TagName("h3"));
            Console.WriteLine(resultTitleElements.Count);

            foreach (IWebElement wel in resultTitleElements)
            {
                Console.WriteLine(wel.Text);
                IWebElement resultUrl = wel.FindElement(By.XPath("//a"));
                String url = resultUrl.GetAttribute("href");
                Console.WriteLine(url);
            }


            driver.Quit();
        }
    }

}
