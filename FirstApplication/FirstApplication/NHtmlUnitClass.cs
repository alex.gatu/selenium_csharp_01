﻿using NHtmlUnit;
using NHtmlUnit.Html;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApplication
{
    [TestFixture]
    public class NHtmlUnitClass
    {
        [Test]
        public void NHtmlUnitDriverTest()
        {
            WebClient htmlUnitDriver = new WebClient(BrowserVersion.CHROME);
            HtmlPage page = htmlUnitDriver.GetHtmlPage("http://www.google.com");
            htmlUnitDriver.Close();
        }

        [Test]
        public void FirefoxDriverTest()
        {
            String path = "E:\\";
            IWebDriver driver = new FirefoxDriver(@path);
            //driver.Url = "http://www.google.com";
            driver.Navigate().GoToUrl("http://www.google.com");
            driver.Navigate().Refresh();
            Console.WriteLine(driver.Title);
            foreach (String handle in driver.WindowHandles)
            {    // switch to the window handle   
                driver.SwitchTo().Window(handle);
                Console.WriteLine(handle);

            }
                driver.Quit();

        }

        [Test]
        public void IeDriverTest()
        {
            String path = "E:\\";
            IWebDriver driver = new InternetExplorerDriver(@path);

            driver.Navigate().GoToUrl("http://www.google.com");
            driver.Close();
        }

        [Test]
        public void EdgeDriverTest()
        {
            String path = "E:\\";
            IWebDriver driver = new EdgeDriver(@path);

            driver.Navigate().GoToUrl("http://www.google.com");
            driver.Quit();
        }


    }
}
