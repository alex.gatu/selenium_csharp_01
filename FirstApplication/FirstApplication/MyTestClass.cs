﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApplication
{
    [TestFixture]
    public class MyTestClass
    {
        string x = "";
        public static IWebDriver driver;

        [OneTimeSetUp]
        public void ClassSetup()
        {
            Console.WriteLine("test fixture setup");
            x = "un string";
            driver = new FirefoxDriver();
            IWebDriver wd = new ChromeDriver();
            wd.Quit();
        }

        [SetUp]
        public void Setup()
        {
            Console.WriteLine("Setup method");
        }

        [Test]
        public void Test1()
        {
            driver.Url = "https://emag.ro";

            Console.WriteLine("test 1 "+ x);
            Shape s = new Shape();
            Assert.AreEqual(10, s.ComputeSum(6, 4));
            driver.Navigate();
        }

        [Test]
        public void Test2()
        {
            Console.WriteLine("test 2");
        }

        [Test]
        public void Test3()
        {
            Console.WriteLine("test 3");
        }

        [TearDown]
        public void Tear()
        {
            Console.WriteLine("tear down");
        }

        [OneTimeTearDown]
        public void ClassTearDown()
        {
            Console.WriteLine("class tear down");
        }
        
    }
}
