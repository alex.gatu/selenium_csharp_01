﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApplication
{
    public class Shape : IDisposable
    {
        public int Height { get; set; }
        public int Width { get; set; }
        string name;

        public void Dispose()
        {
            Console.WriteLine("This is the method that disposes shape objects");
            
        }

        public string Name
        {
            get => name;
            set => name = value;
        }


        public override string ToString()
        {
            return "area is " + base.ToString();
            
        }

        public virtual void ComputeSomething()
        {
            Console.WriteLine("Shape compute method");
        }

        public int ComputeSum(int a, int b)
        {
            return a + b;
        }

        private void ComputePrivate()
        {

        }
    }
}
