﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FirstApplication
{
    class Program
    {
        int test = 2;
        string name;

        struct AnotherStruct
        {
            public MyStruct ms;
            int number;
            public MyStruct getStruct()
            {
                return new MyStruct();
            }
        }

        struct Str
        {
            public string x;

            public Str(string ceva)
            {
                x = ceva;
            }
        }

        class Cls
        {
            public string x;

            public Cls(string ceva)
            {
                x = ceva;
            }
        }

        static void Method1(Str x)
        {
            x = new Str("trei");
        }

        static void Method2(Cls x)
        {
            x = new Cls("patru");
        }

        public enum Browsers
        {
            CHROME,
            FIREFOX,
            IE
        }

        static void Method3()
        {
            //string myString = "Acesta este programul meu!";
            //int x = 2;
            //int y = x + 3;
            //Console.WriteLine(myString + " ceva");
            //DateTime value = new DateTime(2018, 2, 24);
            //Console.WriteLine(value);
            ////int a = x;
            //Console.WriteLine(typeof(char).IsPrimitive);
            //foreach (string s in args)
            //{
            //    Console.WriteLine(s);
            //}

            //AnotherStruct a = new AnotherStruct();
            //MyStruct ms = a.ms;
            //ms.Assign(10, "aa");
            //Console.WriteLine(ms.computeSum());
            //Console.ReadLine();
            Shape s1 = new Shape();
            Console.WriteLine(s1.ToString());
            Circle s2 = new Circle();
            Console.WriteLine(s2.ToString());
            s1.ComputeSomething();
            ((Shape)s2).ComputeSomething();
            Shape[] shapeArr = new Shape[2] {
                s1, s2
            };



            int[] myIntArray = new int[5] { 1, 2, 3, 4, 5 };

            foreach (int val in myIntArray)
            {
                Console.WriteLine(val);
            }

            for (int i = 0; i < myIntArray.Length; i++)
            {
                Console.WriteLine(myIntArray[i]);
            }
            List<string> myList = new List<string>();
            // instead of List<string> you can use var
            myList.Add("Romania");
            myList.Add("R.Moldova");
            myList.Remove("");

            Dictionary<string, string> codes = new Dictionary<string, string>();
            codes.Add("RO", "Romania");
            codes.Add("MD", "Rep.Moldova");

            Console.WriteLine(codes["RO"]);
            foreach (string key in codes.Keys)
            {
                Console.WriteLine("[" + key + "] : " + codes[key]);
            }

            foreach (KeyValuePair<string, string> kvp in codes)
            {
                Console.WriteLine("[" + kvp.Key + "] : " + kvp.Value);
            }
            HashSet<string> hs = new HashSet<string>();

            Dictionary<string, List<string>> countries = new Dictionary<string, List<string>>();
            List<string> europe = new List<string>();

            europe.Add("Romania");
            europe.Add("Rep.Moldova");
            countries.Add("Europe", europe);

            // Convert Array to HashSet
            HashSet<int> hash = new HashSet<int>(myIntArray);

            SortedList<int, string> sortedList1 = new SortedList<int, string>();
            sortedList1.Add(3, "Three");
            sortedList1.Add(4, "Four");
            sortedList1.Add(1, "One");
            sortedList1.Add(5, "Five");

            foreach (KeyValuePair<int, string> kvp in sortedList1)
                Console.WriteLine("key: {0}, value: {1}", kvp.Key, kvp.Value);

            Queue<string> myQ = new Queue<string>();
            myQ.Enqueue("Hello");
            myQ.Enqueue("World");
            myQ.Enqueue("!");
            foreach (Object obj in myQ)
                Console.Write(" {0}", obj);

            Stack<string> myS = new Stack<string>();
            myS.Push("Hello");
            myS.Push("World");
            myS.Push("!");
            foreach (Object obj in myS)
                Console.Write(" {0}", obj);
        }

        static void Main(string[] args)
        {

            try
            {
                int number = int.Parse(args[0]);
                Console.WriteLine("Number parsed is {0}", number);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error encountered " + ex.GetType());
            }
            finally
            {
                Console.WriteLine("Finally executes!! ");
            }

            try
            {
                if (!File.Exists("ceva.txt"))
                {
                    throw new FileNotFoundException();
                }
            }
            catch { }

            GenExc1(new int[4] { 1, 2, 3, 5 });
            GenExc1(new int[5] { 1, 2, 3, 5, 5 });

            //Shape mShape = null;
            //try
            //{
            //    mShape = new Shape();
            //    // 
            //    mShape.ComputeSomething();
            //}
            //finally
            //{
            //    mShape.Dispose();
            //}

            using (Shape s1 = new Shape())
            {
                s1.ComputeSomething();
            }

            try
            {
                Console.WriteLine(SafeDivide(1, 0));
                Console.WriteLine(SafeDivide(1, 5));
                Console.WriteLine(SafeDivide(10, 5));


            }
            catch (DivideByZeroException dbz)
            {
                Console.WriteLine(dbz.Message);
            }
            catch { }
            //int x = 0;
            //Console.WriteLine("ceva {0}", x);

            //try
            //{
            //    TestThrow();
            //}
            //catch (CustomException cex)
            //{
            //    Console.WriteLine(cex);
            //}


            // try/catch the method which passes the exception with break stack
            try
            {
                Throw2();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.WriteLine();

            // try/catch the method which passes the exception with preserve stack
            try
            {
                Throw1();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }



            Console.ReadKey();
        }

        public static void Throw1()
        {
            Console.WriteLine("This is the method which retrows");
            try
            {
                TestThrow();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static void Throw2()
        {
            Console.WriteLine("This is the method which breaks the stack");
            try
            {
                TestThrow();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void TestThrow()
        {
            CustomException cex = new CustomException(" this is an exceptional case !!!");
            throw cex;
        }

        public static double SafeDivide(double x, double y)
        {
            if (y==0)
            {
                throw new DivideByZeroException("This is my custom message");
            }
            return x / y;
        }

        public static void GenExc1(int[] x)
        {
            try
            {
                Console.WriteLine(x[4]);
            }
            catch (IndexOutOfRangeException ior)
            {
                Console.WriteLine(ior.Message);
            }
        }
    }
}
